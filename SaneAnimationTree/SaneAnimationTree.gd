# SaneAnimationTree #
# Adds a collection of functions to make interaction with the AnimationTree similar to the AnimationTreePlayer
# and overall a less bizarre and frustrating experience.

extends AnimationTree
class_name SaneAnimationTree, "res://SaneAnimationTree/tree_icon.png"

func node_exists(node:String):
	if(tree_root.get_node(node) == null):
		push_error("Failed to find node '"+node+"'.")
		breakpoint
func node_assert_type(node:String, type):
	if(tree_root.get_node(node) is type):
		return true
	else:
		push_error("Node '"+node+"' was not of the expected type '"+str(type)+"'")
		breakpoint
func assert_node(node:String, type):
	node_exists(node)
	node_assert_type(node, type)

# Animation Node #
# Returns animation name used in animation node
func animation_get(node:String)->String:
	assert_node(node, AnimationNodeAnimation)
	return tree_root.get_node(node).get_animation()
# Sets the animation via name (from given AnimationPlayer library)
func animation_set(node:String, anim:String)->void:
	assert_node(node, AnimationNodeAnimation)
	if(get_node(anim_player).get_animation(anim)):
		tree_root.get_node(node).set_animation(anim)
	else:
		push_error("animation_set: Could not find animation by name: '"+anim+"'")
		breakpoint

# Add2 Node #
func add2_get_amount(node:String)->float:
	assert_node(node, AnimationNodeAdd2)
	return get("parameters/"+node+"/add_amount")
func add2_set_amount(node:String, add_amount:float)->void:
	assert_node(node, AnimationNodeAdd2)
	if(add_amount < 0 or add_amount > 1):
		push_error("add2_set_amount: add_amount given ( "+str(add_amount)+" ) was out of valid range.")
		add_amount = clamp(add_amount, 0, 1)
	set("parameters/"+node+"/add_amount", add_amount)

func add2_get_sync(node:String)->bool:
	assert_node(node, AnimationNodeAdd2)
	return tree_root.get_node(node).is_using_sync()
func add2_set_sync(node:String, use_sync:bool)->void:
	assert_node(node, AnimationNodeAdd2)
	tree_root.get_node(node).set_use_sync(use_sync)

# Add3 Node #
func add3_get_amount(node:String)->float:
	assert_node(node, AnimationNodeAdd3)
	return get("parameters/"+node+"/add_amount")
func add3_set_amount(node:String, add_amount:float)->void:
	assert_node(node, AnimationNodeAdd3)
	if(add_amount < -1 or add_amount > 1):
		push_error("add3_set_amount: add_amount given ( "+str(add_amount)+" ) was out of valid range.")
		add_amount = clamp(add_amount, -1, 1)
	set("parameters/"+node+"/add_amount", add_amount)

func add3_get_sync(node:String)->bool:
	assert_node(node, AnimationNodeAdd3)
	return tree_root.get_node(node).is_using_sync()
func add3_set_sync(node:String, use_sync:bool)->void:
	assert_node(node, AnimationNodeAdd3)
	tree_root.get_node(node).set_use_sync(use_sync)

# Blend2 Node #
func blend2_get_amount(node:String)->float:
	assert_node(node, AnimationNodeBlend2)
	return get("parameters/"+node+"/blend_amount")
func blend2_set_amount(node:String, blend_amount:float)->void:
	assert_node(node, AnimationNodeBlend2)
	if(blend_amount < 0 or blend_amount > 1):
		push_error("blend2_set_amount: blend_amount given ( "+str(blend_amount)+" ) was out of valid range.")
		blend_amount = clamp(blend_amount, 0, 1)
	set("parameters/"+node+"/blend_amount", blend_amount)

func blend2_get_sync(node:String)->bool:
	assert_node(node, AnimationNodeBlend2)
	return tree_root.get_node(node).is_using_sync()
func blend2_set_sync(node:String, use_sync:bool)->void:
	assert_node(node, AnimationNodeBlend2)
	tree_root.get_node(node).set_use_sync(use_sync)

# Blend3 Node #
func blend3_get_amount(node:String)->float:
	assert_node(node, AnimationNodeBlend3)
	return get("parameters/"+node+"/blend_amount")
# Amount is between -1 to 1
func blend3_set_amount(node:String, blend_amount:float)->void:
	assert_node(node, AnimationNodeBlend3)
	if(blend_amount < -1 or blend_amount > 1):
		push_error("blend3_set_amount: blend_amount given ( "+str(blend_amount)+" ) was out of valid range.")
		blend_amount = clamp(blend_amount, -1, 1)
	set("parameters/"+node+"/blend_amount", blend_amount)

func blend3_get_sync(node:String)->bool:
	assert_node(node, AnimationNodeBlend3)
	return tree_root.get_node(node).is_using_sync()
func blend3_set_sync(node:String, use_sync:bool)->void:
	assert_node(node, AnimationNodeBlend3)
	tree_root.get_node(node).set_use_sync(use_sync)

# Timescale Node #

func timescale_get_scale(node:String)->float:
	assert_node(node, AnimationNodeTimeScale)
	return get("parameters/"+node+"/scale")
func timescale_set_scale(node:String, scale:float)->void:
	assert_node(node, AnimationNodeTimeScale)
	set("parameters/"+node+"/scale", scale)

# Timeseek Node #

func timeseek_get_pos(node:String)->float:
	assert_node(node, AnimationNodeTimeSeek)
	return get("parameters/"+node+"/seek_position")
func timeseek_set_pos(node:String, seek_to:float)->void:
	assert_node(node, AnimationNodeTimeSeek)
	set("parameters/"+node+"/seek_position", seek_to)

# Oneshot node #
# Fires the node to play its animation
func oneshot_play(node:String)->void:
	assert_node(node, AnimationNodeOneShot)
	if(not get("parameters/"+node+"/active")):
		set("parameters/"+node+"/active", true)
	else:
		set("parameters/"+node+"/remaining", get("parameters/"+node+"/time"))

func oneshot_get_do_autorestart(node:String)->bool:
	assert_node(node, AnimationNodeOneShot)
	return tree_root.get_node(node).has_autorestart()
func oneshot_set_do_autorestart(node:String, do_autorestart)->void:
	assert_node(node, AnimationNodeOneShot)
	tree_root.get_node(node).set_autorestart(do_autorestart)

func oneshot_get_autorestart_delay(node:String)->float:
	assert_node(node, AnimationNodeOneShot)
	return tree_root.get_node(node).get_autorestart_delay()
func oneshot_set_autorestart_delay(node:String, autorestart_delay)->void:
	assert_node(node, AnimationNodeOneShot)
	tree_root.get_node(node).set_autorestart_delay(autorestart_delay)

func oneshot_get_autorestart_random_delay(node:String)->float:
	assert_node(node, AnimationNodeOneShot)
	return tree_root.get_node(node).get_autorestart_random_delay()
func oneshot_set_autorestart_random_delay(node:String, autorestart_random_delay)->void:
	assert_node(node, AnimationNodeOneShot)
	tree_root.get_node(node).set_autorestart_random_delay(autorestart_random_delay)

func oneshot_get_fadein(node:String)->float:
	assert_node(node, AnimationNodeOneShot)
	return tree_root.get_node(node).get_fadein_time()
func oneshot_set_fadein(node:String, in_time:float)->void:
	assert_node(node, AnimationNodeOneShot)
	tree_root.get_node(node).set_fadein_time(in_time)
	
func oneshot_get_fadeout(node:String)->float:
	assert_node(node, AnimationNodeOneShot)
	return tree_root.get_node(node).get_fadeout_time()
func oneshot_set_fadeout(node:String, out_time:float)->void:
	assert_node(node, AnimationNodeOneShot)
	tree_root.get_node(node).set_fadeout_time(out_time)

func oneshot_get_sync(node:String)->bool:
	assert_node(node, AnimationNodeOneShot)
	return tree_root.get_node(node).is_using_sync()
func oneshot_set_sync(node:String, use_sync:bool)->void:
	assert_node(node, AnimationNodeOneShot)
	tree_root.get_node(node).set_use_sync(true)

# Transition Node #

func transition_get_current(node:String)->int:
	assert_node(node, AnimationNodeTransition)
	return get("parameters/"+node+"/current")
func transition_set_current(node:String, input_idx:int)->void:
	assert_node(node, AnimationNodeTransition)
	set("parameters/"+node+"/current", input_idx)
	
func transition_get_inputs(node:String)->int:
	assert_node(node, AnimationNodeTransition)
	return tree_root.get_node(node).get_enabled_inputs()
func transition_set_inputs(node:String, input_count:int)->void:
	assert_node(node, AnimationNodeTransition)
	tree_root.get_node(node).set_enabled_inputs(input_count)
	
func transition_get_xfade_time(node:String)->float:
	assert_node(node, AnimationNodeTransition)
	return tree_root.get_node(node).xfade_time
func transition_set_xfade_time(node:String, period:float)->void:
	assert_node(node, AnimationNodeTransition)
	tree_root.get_node(node).xfade_time = period
